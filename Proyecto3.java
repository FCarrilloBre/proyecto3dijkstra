/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto3;
import java.util.*;

public class Proyecto3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int modelo = 0;
        int n,m;
        Boolean Autci;
        while (modelo != 5) {
            System.out.println("Elige el modelo que quieres:");
            System.out.println("1.- Erdös-Renyi");
            System.out.println("2.- Modelo de Gilbert");
            System.out.println("3.- Modelo geografico simple");
            System.out.println("4.- Variante del modelo Barabasi Albert");
            System.out.println("5.- Salir");
            modelo = sc.nextInt();
            switch (modelo) {
                case 1:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    String name = sc.nextLine();
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce el número de parejas posibles:");
                    m = sc.nextInt();
                    System.out.println("Introduce el límite inferior para el rango de los pesos:");
                    double r1 = sc.nextDouble();
                    System.out.println("Introduce el límite superior para el rango de los pesos:");
                    double r2 = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    boolean dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    int root = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de grafo con pesos .gv");
                    String name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo del algoritmo Dijkstra .gv");
                    String name3 =sc.nextLine();
                    GrafoER uno=new GrafoER(n);
                    uno.modeloER(m,dirig,Autci);
                    uno.escribirArchivo(name);
                    GrafoER unoweighted=uno.EdgeValues(r1,r2);
                    unoweighted.escribirArchivo(name2);
                    GrafoER unodijkstra=unoweighted.Dijkstra(root);
                    unodijkstra.escribirArchivo(name3);
                    break;
                case 2:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    name = sc.nextLine();
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce la probabilidad de que esten conectados");
                    double p = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    root = sc.nextInt();
                    System.out.println("Introduce el límite inferior para el rango de los pesos:");
                    r1 = sc.nextDouble();
                    System.out.println("Introduce el límite superior para el rango de los pesos:");
                    r2 = sc.nextDouble();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de grafo con pesos .gv");
                    name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo del resultado de Dijkstra .gv");
                    name3 =sc.nextLine();
                    GrafoGil dos=new GrafoGil(n);
                    dos.modeloGilbert(p,dirig,Autci);
                    dos.escribirArchivo(name);
                    GrafoGil dosweigh= dos.EdgeValues(r1, r2);
                    dosweigh.escribirArchivo(name2);
                    GrafoGil dosdijkstra=dosweigh.Dijkstra(root);
                    dosdijkstra.escribirArchivo(name3);
                    break;
                case 3:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce la distancia:");
                    double r = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    root = sc.nextInt();
                    System.out.println("Introduce el límite inferior para el rango de los pesos:");
                    r1 = sc.nextDouble();
                    System.out.println("Introduce el límite superior para el rango de los pesos:");
                    r2 = sc.nextDouble();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de grafo con pesos .gv");
                    name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DIJKSTRA .gv");
                    name3 =sc.nextLine();
                    Grafogeo tres=new Grafogeo(n);
                    tres.modeloGeoSimple(r,dirig,Autci);
                    tres.escribirArchivo(name);
                    Grafogeo tresweigh= tres.EdgeValues(r1, r2);
                    tresweigh.escribirArchivo(name2);
                    Grafogeo tresdijkstra=tresweigh.Dijkstra(root);
                    tresdijkstra.escribirArchivo(name3);
                    break;
                case 4:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce el grado máximo por vertice:");
                    int grado = sc.nextInt();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    root = sc.nextInt();
                    System.out.println("Introduce el límite inferior para el rango de los pesos:");
                    r1 = sc.nextDouble();
                    System.out.println("Introduce el límite superior para el rango de los pesos:");
                    r2 = sc.nextDouble();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu grafo con pesos .gv");
                    name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo del algoritmo DIJKSTRA .gv");
                    name3 =sc.nextLine();
                    GrafoBA cuatro=new GrafoBA(n);
                    cuatro.modeloBA(grado,dirig);
                    cuatro.escribirArchivo(name);
                    GrafoBA fourweigh= cuatro.EdgeValues(r1, r2);
                    fourweigh.escribirArchivo(name2);
                    GrafoBA fourdijkstra=fourweigh.Dijkstra(root);
                    fourdijkstra.escribirArchivo(name3);
                    break;
                default:
                    break;
    }
        }       
  }
    
}
