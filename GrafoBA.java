
package proyecto3;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.*;

public class GrafoBA {
    private Vertice[] nodes;
  private HashMap<Vertice, HashSet<Vertice>> graph;
  private final int numeroVertices; //número de vértices del grafo
  private int numeroAristas;  //número de aristas únicas del grafo
  private static Formatter output; //objeto para escribir a disco
  private HashMap<Vertice, HashSet<Arista>> incidencia; //mapa para Dijkstra
  private Boolean weighted; //bandera a usar si grafo es pesado


  //////////Constructores//////////

  public GrafoBA(int numVertices) {
    this.graph = new HashMap<Vertice, HashSet<Vertice>>();
    this.numeroVertices = numVertices;
    this.nodes = new Vertice[numVertices];
    this.incidencia = new HashMap<Vertice, HashSet<Arista>>();
    for (int i = 0; i < numVertices; i++) {
      Vertice n = new Vertice(i);
      this.nodes[i] = n;
      this.graph.put(n, new HashSet<Vertice>());
      this.incidencia.put(n, new HashSet<Arista>());
        }
      this.weighted = false;
  }
  
  public int gradoVertice(int i) {
    Vertice n1 = this.getNode(i);
    return this.graph.get(n1).size();
  }

  public void conectarVertices(int i, int j) {
    /*Se recuperan los vértices de los índices i y j*/
     Vertice n1 = this.getNode(i);
     Vertice n2 = this.getNode(j);
     /*Se recuperan las aristas de cada vértice*/
     HashSet<Vertice> aristas1 = this.getEdges(i);
     HashSet<Vertice> aristas2 = this.getEdges(j);

     /*Se agregan los vértices al conjunto del otro*/
     aristas1.add(n2);
     aristas2.add(n1);  
     this.numeroAristas +=1;
     
  }
  
   public void conectarVertices2(int i, int j) {
    /*Se recuperan los vértices de los índices i y j*/
     Vertice n1 = this.getNode(i);
     Vertice n2 = this.getNode(j);
     /*Se recuperan las aristas de cada vértice*/
     HashSet<Vertice> aristas1 = this.getEdges(i);
     HashSet<Vertice> aristas2 = this.getEdges(j);

     /*Se agregan los vértices al conjunto del otro*/
     aristas1.add(n2); 
     this.numeroAristas +=1;
     
  }

  //Regresa 'true' si ya existe la arista
  private Boolean existeConexion(int i, int j) {
    /*Se recuperan los vértices de los índices i y j*/
     Vertice n1 = this.getNode(i);
     Vertice n2 = this.getNode(j);
    /*Se recuperan las aristas de cada vértice*/
    HashSet<Vertice> aristas1 = this.getEdges(i);
    HashSet<Vertice> aristas2 = this.getEdges(j);
    /*Se revisa que un nodo esté en el conjunto de aristas del otro*/
     if (aristas1.contains(n2) || aristas2.contains(n1)) {
       return true;
     }
     else{
       return false;
     }
  }

  //////////getters/setters de las variables de instancia//////////
  public int getNumNodes() {return numeroVertices;}

  public int getNumEdges() {return numeroAristas;}

  public Vertice getNode(int i) {return this.nodes[i];}


  public HashSet<Vertice> getEdges(int i) {
    Vertice n = this.getNode(i);
    return this.graph.get(n);
  }

 public Boolean getWeightedFlag() {return this.weighted;}

  public HashSet<Arista> getWeightedEdges(int i) {
    Vertice n = this.getNode(i);
    return this.incidencia.get(n);
  }

  public void setWeighted() {this.weighted = true;}

  public void setIncidencia(int i, HashSet<Arista> aristasPeso) {
    this.incidencia.put(this.getNode(i), aristasPeso);}


  //////////Método toString para representación en String del GrafoER//////////
  public String toString() {
     String salida;
    if (this.getWeightedFlag()) { //esta parte es para grafos pesados y con
      salida ="graph {\n";        // nodos etiquetados
      for (int i = 0; i < this.getNumNodes(); i++) {
        salida += this.getNode(i).getName() + " [label=\""
        + this.getNode(i).getName() + " ("+ this.getNode(i).getDistance()
        + ")\"];\n";
      }
      for (int i = 0; i < this.getNumNodes(); i++) {
        HashSet<Arista> aristas = this.getWeightedEdges(i);
        for (Arista e : aristas) {
        salida += e.getNode1() + " -- " + e.getNode2()
        + " [weight=" + e.getWeight()+"" + " label="+e.getWeight()+""
        + "];\n";
        }
       }
      salida += "}\n";
    }
    else { //esta es para grafos sin pesos ni etiquetas
      salida ="graph {\n";
      for (int i = 0; i < this.getNumNodes(); i++) {
        salida += this.getNode(i).getName() + ";\n";
      }
      for (int i = 0; i < this.getNumNodes(); i++) {
        HashSet<Vertice> aristas = this.getEdges(i);
        for (Vertice n : aristas) {
        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
        }
       }
      salida += "}\n";
    }
    return salida;
  }
  
  public void modeloBA(int d,boolean dirigido) {
      Random volado=new Random();
      if (dirigido==true){
        for(int i = 0; i < d; i++){
            for(int j = 0; j < i; j++) {
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j);
                }
            }
        }
        for(int i = d; i < this.getNumNodes();) {
            for(int j = 0; j < i; j++) {
                double probabilidad =(double)gradoVertice(j)/(double)this.getNumEdges();
                if (volado.nextDouble() <= probabilidad) {
                    if (!existeConexion(i, j) && (gradoVertice(i) < d)) {
                        conectarVertices(i, j);
                    }
                }
            }
            if (gradoVertice(i) >= d) i++;
        }
    }
      if (dirigido==false){
        for(int i = 0; i < d; i++){
            for(int j = 0; j < i; j++) {
                if (!existeConexion(i, j)) {
                    conectarVertices2(i, j);
                }
            }
        }
        for(int i = d; i < this.getNumNodes();) {
            for(int j = 0; j < i; j++) {
                double probabilidad =(double)gradoVertice(j)/(double)this.getNumEdges();
                if (volado.nextDouble() <= probabilidad) {
                    if (!existeConexion(i, j) && (gradoVertice(i) < d)) {
                        conectarVertices2(i, j);
                    }
                }
            }
            if (gradoVertice(i) >= d) i++;
        }
    }
  }
  
public void escribirArchivo(String nombre) {
    try{
      output = new Formatter(nombre);
    }
    catch (SecurityException securityException) {
      System.err.println("No hay permiso de escritura.");
      System.exit(1);
    }
    catch (FileNotFoundException fileNotFoundException) {
      System.err.println("Error al abrir el archivo.");
      System.exit(1);
    }
    try{
      output.format("%s",this);
    }
    catch (FormatterClosedException formatterClosedException) {
      System.err.println("Error al escribir al archivo");
    }
    if (output != null)
    output.close();
  }

  public void setAristaPeso(int i, int j, double peso) {
    if (!this.existeConexion(i, j)) this.conectarVertices(i, j);
    Arista aristaNuevaij = new Arista(i, j, peso);
    Arista aristaNuevaji = new Arista(j, i, peso);
    HashSet<Arista> aristasNodoi = this.getWeightedEdges(i);
    HashSet<Arista> aristasNodoj = this.getWeightedEdges(j);
    aristasNodoi.add(aristaNuevaij);
    aristasNodoj.add(aristaNuevaji);
    this.setIncidencia(i, aristasNodoi);
    this.setIncidencia(j, aristasNodoj);
    if (!this.getWeightedFlag()) this.setWeighted();
  }

  ////////// TERCERA ENTREGA //////////

  ///////// Método para asignar pesos a las aristas entre dos valores /////////
  public GrafoBA EdgeValues(double min, double max) {
    GrafoBA grafoPesado = new GrafoBA(this.getNumNodes()); //grafo de salida
    Random rand = new Random();
    double peso;
    for (int i = 0; i < this.getNumNodes(); i++) {  //se recorre el grafo
      for (int j = i; j < this.getNumNodes(); j++) { //original
        if(this.existeConexion(i, j)) { //donde existe conexión entre nodos
          //se crea una arista con peso aleatorio en el grafo de salida
          peso = rand.nextFloat()*(max - min) + min;
          grafoPesado.setAristaPeso(i, j, peso);
        }
      }
    }
    return grafoPesado;
  }

  public GrafoBA Dijkstra(int s) {
    GrafoBA arbol = new GrafoBA(this.getNumNodes()); //grafo de salida
    double inf = Double.POSITIVE_INFINITY;  // máxima distancia
    //arreglo donde se guarda el nodo padre de cada nodo
    Integer[] padres = new Integer[arbol.getNumNodes()];
    // Las distancias de todos los nodos a infinito y todos los padres a null
    for (int i = 0; i < arbol.getNumNodes(); i++) {
      this.getNode(i).setDistance(inf);
      padres[i] = null;
    }
    // la distancia del nodo fuente a 0.0 y a sí mismo como padre
    this.getNode(s).setDistance(0.0);
    padres[s] = s;
    // Cola de prioridad de los nodos. La llave es la distancia
    PriorityQueue<Vertice> distPQ = new PriorityQueue<>(vertexDistanceComp);
    for (int i = 0; i < this.getNumNodes(); i++) {
        distPQ.add(this.getNode(i));
    }
    //Se explora el grafo
    while (distPQ.peek() != null) {  // se revisa que no esté vacía la cola
      Vertice u = distPQ.poll();  // elemento de la cola de prioridad
      //aristas del nodo u
      HashSet<Arista> aristas = this.getWeightedEdges(u.getIndex());
      for (Arista e : aristas) {
        // actualizar padre y distancia si es menor a la que tenía
        if(this.getNode(e.getIntN2()).getDistance() > this.getNode(u.getIndex()).getDistance() + e.getWeight()) {
          this.getNode(e.getIntN2()).setDistance(this.getNode(u.getIndex()).getDistance() + e.getWeight());
          padres[e.getIntN2()] = u.getIndex();
        }
      }
    }
    //se conecta el árbol de salida con la lista de padres y se asigna la
    //distancia al nodo fuente a cada nodo
    
    System.out.println(arbol);
    System.out.println(arbol.getNumNodes());
    for (int i = 0; i < arbol.getNumNodes(); i++) {
      System.out.println(padres[i]);
      arbol.setAristaPeso(i, padres[i],1.0);
      arbol.getNode(i).setDistance(this.getNode(i).getDistance());
    }
    return arbol;
  }
  //comparador necesario para la cola de prioridad de objetos 'Vertice'
  Comparator<Vertice> vertexDistanceComp = new Comparator<Vertice>() {
              @Override
              public int compare(Vertice n1, Vertice n2) {
                  return Double.compare(n1.getDistance(), n2.getDistance());
              }
          };
}